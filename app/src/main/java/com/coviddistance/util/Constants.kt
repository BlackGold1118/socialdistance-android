package com.coviddistance.util

object Constants {
    const val PREF_FILE_NAME = "com.coviddistance.preferencesV2"
    const val PREF_IS_ONBOARDED = "is_onboarded"

    const val QR_CODE_SIZE = 200
}